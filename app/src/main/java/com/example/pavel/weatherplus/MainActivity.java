package com.example.pavel.weatherplus;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import static com.example.pavel.weatherplus.Utilities.isDay;


public class MainActivity extends Activity {

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity.context = getApplicationContext();

        final ActionBar actionBar = this.getActionBar();

        //Apply the appropriate theme
        if (isDay()) {
            this.setTheme(R.style.DayTheme);
            if (actionBar != null) {
                //Replace title and background with custom logo
                actionBar.setDisplayShowTitleEnabled(false);
                BitmapDrawable background = new BitmapDrawable (
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.img_actionbar_logo_day));
                actionBar.setBackgroundDrawable(background);
            }
        }
        else {
            this.setTheme(R.style.NightTheme);
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                BitmapDrawable background = new BitmapDrawable (
                        BitmapFactory.decodeResource(getResources(),
                                R.drawable.img_actionbar_logo_night));
                actionBar.setBackgroundDrawable(background);
            }
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new WeatherFragment())
                    .commit();
        }
    }

    protected static Context getAppContext(){
        return MainActivity.context;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_map_location) {
            viewLocationInMap();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void viewLocationInMap() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String locationKey = preferences.getString(getString(R.string.settings_location), getString(R.string.settings_location_default));
        Uri mapLocation = Uri.parse("geo:0,0").buildUpon().appendQueryParameter("q", locationKey).build();
        Log.v("MainActivity", "mapLocation: " + mapLocation);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(mapLocation);

        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
        else
            Log.d("MainActivity", "No app to receive " + locationKey);
    }

}
