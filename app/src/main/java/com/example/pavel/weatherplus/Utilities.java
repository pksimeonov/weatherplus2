package com.example.pavel.weatherplus;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Formatting and handling of data
 */
public class Utilities {

    private static ConnectivityManager connectivity
            = (ConnectivityManager) MainActivity.getAppContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE);

    //Check fo network connection
    public static Boolean checkInternetState() {
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    //convert the date from unix time to easily readable format
    public static String convertDate(long time) {
        Date date = new Date(time * 1000);
        SimpleDateFormat simpleFormat = new SimpleDateFormat("EEE, d MMM");
        return simpleFormat.format(date);
    }

    //Returns the selected unit type - true if Metric, false if Imperial
    public static boolean unitType(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(context.getString(R.string.settings_units_types),
                context.getString(R.string.settings_units_metric))
                .equals(context.getString(R.string.settings_units_metric));
    }

    //Format the temperature for the upper part of the fragment (today's details)
    public static String formatTemperatureToday(Context context, double temperature) { //this formatting is for the Today section of the app
        if (Utilities.unitType(context)) {
            return context.getString(R.string.format_temperature_metric, temperature);
        }
        else {
            temperature = 9*temperature/5+32;
            return context.getString(R.string.format_temperature_imperial, temperature);
        }
    }

    //Format the temperature for the forecast list view
    public static String formatTemperature (Context context, double low, double high) {
        if (Utilities.unitType(context)) {
            return Long.toString(Math.round(high)) + "/" + Long.toString(Math.round(low)); //example (High / low temperature): "12 / 9"
        }
        else {
            return Long.toString(Math.round(9*high/5+32)) + "/" + Long.toString(Math.round(9*low/5+32));
        }
    }

    //Format the wind string
    public static String formatWind(Context context, double windSpeed, double degrees) {
        int windFormat;

        //Apply xml custom format depending on unit type (metric or imperial)
        if (Utilities.unitType(context)) {
            windFormat = R.string.format_wind_kmh;
        } else {
            windFormat = R.string.format_wind_mph;
            windSpeed = .621371192237334f * windSpeed;
        }

        // From wind direction in degrees, determine compass direction as a string (e.g NW)
        String direction = "Unknown";
        if (degrees >= 337.5 || degrees < 22.5) {
            direction = "N";
        } else if (degrees >= 22.5 && degrees < 67.5) {
            direction = "NE";
        } else if (degrees >= 67.5 && degrees < 112.5) {
            direction = "E";
        } else if (degrees >= 112.5 && degrees < 157.5) {
            direction = "SE";
        } else if (degrees >= 157.5 && degrees < 202.5) {
            direction = "S";
        } else if (degrees >= 202.5 && degrees < 247.5) {
            direction = "SW";
        } else if (degrees >= 247.5 && degrees < 292.5) {
            direction = "W";
        } else if (degrees >= 292.5 || degrees < 22.5) {
            direction = "NW";
        }
        return String.format(context.getString(windFormat), windSpeed, direction);
    }

    //Take current time, and if it's between 7 and 19:00 - return true
    public static Boolean isDay() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("k");
        String formattedTime = simpleDateFormat.format(now);
        int day = Integer.parseInt(formattedTime);
        return day >= 7 && day < 19;
    }

    //Returns a weather icon depending on the weather code received from the API
    public static int getWeatherIcon(String weatherCodeStr) {
        int weatherCode = Integer.parseInt(weatherCodeStr);
        //isDay returns true if the current time is between 07:00 and 17:00
        if (isDay()) {
            //WeatherCode contains the weatherID from the API's response
            //The following icons are used during day time
            switch (weatherCode) {
                case 200: //light/med thunderstorm
                case 201:
                case 210:
                case 211:
                case 230:
                case 231:
                    return R.drawable.img_thunderstorm_day;
                case 202: //thunderstorm
                case 212:
                case 232:
                    return R.drawable.img_thunderstorm_heavy_day;
                case 221: //ragged thunderstorm
                    return R.drawable.img_thunderstorm;
                //rain
                case 302:
                case 312:
                case 314:
                    return R.drawable.img_rain_heavy_day;
                case 300:
                case 301:
                case 310:
                case 311:
                case 313:
                case 321:
                    return R.drawable.img_rain_light_day;
                case 502:
                case 503:
                case 504:
                case 511:
                case 522:
                case 531:
                    return R.drawable.img_rain_heavy_day;
                case 500:
                case 501:
                case 520:
                case 521:
                    return R.drawable.img_rain_light_day;
                case 600:
                case 611:
                case 620:
                    return R.drawable.img_snow_light_day;
                case 601:
                case 612:
                case 615:
                case 621:
                    return R.drawable.img_snow_medium_day;
                case 602:
                case 616:
                case 622:
                    return R.drawable.img_snow_heavy_day;
                case 701: // mist
                case 711: // smoke
                case 721: // haze
                case 731: // sand/dust whirls
                case 741: // fog
                case 751: // sand
                case 761: // dust
                    return R.drawable.img_fog_day;
                case 762: // volcanic ash
                case 771: // squalls
                case 781: // tornado
                    return R.drawable.img_thunderstorm;
                case 800:
                    return R.drawable.img_clear_day;
                case 801:
                    return R.drawable.img_cloudy_light_day;
                case 802:
                    return R.drawable.img_cloudy_medium_day;
                case 803:
                    return R.drawable.img_cloudy_heavy_day;
                case 804:
                    return R.drawable.img_cloudy_very_heavy_day;
                default:
                    return R.drawable.img_unknown;
            }
        }
        else {
            //Night time icons
            switch (weatherCode) {
                case 200: //light/med thunderstorm
                case 201:
                case 210:
                case 211:
                case 230:
                case 231:
                    return R.drawable.img_thunderstorm_night;
                case 202: //thunderstorm
                case 212:
                case 232:
                    return R.drawable.img_thunderstorm_heavy_night;
                case 221: //ragged thunderstorm
                    return R.drawable.img_thunderstorm;
                //rain
                case 302:
                case 312:
                case 314:
                    return R.drawable.img_rain_heavy_night;
                case 300:
                case 301:
                case 310:
                case 311:
                case 313:
                case 321:
                    return R.drawable.img_rain_light_night;
                case 502:
                case 503:
                case 504:
                case 511:
                case 522:
                case 531:
                    return R.drawable.img_rain_heavy_night;
                case 500:
                case 501:
                case 520:
                case 521:
                    return R.drawable.img_rain_light_night;
                case 600:
                case 611:
                case 620:
                    return R.drawable.img_snow_light_night;
                case 601:
                case 612:
                case 615:
                case 621:
                    return R.drawable.img_snow_medium_night;
                case 602:
                case 616:
                case 622:
                    return R.drawable.img_snow_heavy_night;
                case 701: // mist
                case 711: // smoke
                case 721: // haze
                case 731: // sand/dust whirls
                case 741: // fog
                case 751: // sand
                case 761: // dust
                    return R.drawable.img_fog_night;
                case 762: // volcanic ash
                case 771: // squalls
                case 781: // tornado
                    return R.drawable.img_thunderstorm;
                case 800:
                    return R.drawable.img_clear_night;
                case 801:
                    return R.drawable.img_cloudy_light_night;
                case 802:
                    return R.drawable.img_cloudy_medium_night;
                case 803:
                    return R.drawable.img_cloudy_heavy_night;
                case 804:
                    return R.drawable.img_cloudy_very_heavy_night;
                default:
                    return R.drawable.img_unknown;
            }
        }
    }
}
