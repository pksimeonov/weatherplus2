package com.example.pavel.weatherplus;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static com.example.pavel.weatherplus.Utilities.convertDate;
import static com.example.pavel.weatherplus.Utilities.formatTemperature;
import static com.example.pavel.weatherplus.Utilities.formatTemperatureToday;
import static com.example.pavel.weatherplus.Utilities.formatWind;
import static com.example.pavel.weatherplus.Utilities.getWeatherIcon;
import static com.example.pavel.weatherplus.Utilities.isDay;

//A weather forecast fragment containing a simple view.
public class WeatherFragment extends Fragment {
    static String todayDate;
    static String todayWeatherDescription;
    static String todayHumidity;
    static String todayPressure;
    static String todayWind;
    static String todayClouds;
    static String todayTemperatureHigh;
    static String todayTemperatureLow;
    static String todayWeatherCode;

    ArrayAdapter<String> forecastAdapter;
    String settingsLocation;
    static String settingsUnits;

    public WeatherFragment() {

    }

    public void updateWeatherData() {
        getWeatherData weatherData = new getWeatherData();
        weatherData.execute(settingsLocation);
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(getActivity());
        settingsLocation = preference.getString(getString(R.string.settings_location), getString(R.string.settings_location_default));
        settingsUnits = preference.getString(getString(R.string.settings_units_types),
                getString(R.string.settings_units_metric));
        //If there is network connection
        if (Utilities.checkInternetState()) {
            updateWeatherData();
        }
        else {
            Toast.makeText(MainActivity.getAppContext(),
                    "Internet connection required!",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_refresh) {
            updateWeatherData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        //Array adapter used to populate the listView
        forecastAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textView,
                new ArrayList<String>());

        //Attach the adapter to the listView
        ListView listView = (ListView) rootView.findViewById(R.id.listView_forecast);
        listView.setAdapter(forecastAdapter);

        if (isDay()) {
            //Apply background image
            rootView.setBackgroundResource(R.drawable.background_day);
            //Apply listView background color
            listView.setBackgroundColor(Color.parseColor("#80ffffff"));
        }
        else {
            rootView.setBackgroundResource(R.drawable.background_night);
            listView.setBackgroundColor(Color.parseColor("#79303030"));
        }

        return rootView;
    }

    public class getWeatherData extends AsyncTask<String, Void, String[]> {
        //Put the class name in the log_title variable, which will make locating the error more easy
        //Using class.getSimpleName, as it will throw an exception if the class name is changed, without changing it here as well
        private final String log_title = getWeatherData.class.getSimpleName();

        private String[] extractJsonData(String weatherJsonStr, int daysNum) throws JSONException {
            //The JSON contains the following objects:
            final String LIST = "list";
            final String DATETIME = "dt";
            final String TEMPERATURE = "temp";
            final String PRESSURE = "pressure";
            final String HUMIDITY = "humidity";
            final String WIND_SPEED = "speed";
            final String WIND_DIRECTION = "deg";
            final String CLOUDS = "clouds";
            final String MIN = "min";
            final String MAX = "max";
            final String WEATHER = "weather";
            final String WEATHER_DESCRIPTION = "description";
            final String WEATHER_CODE = "id";

            JSONObject weatherJson = new JSONObject(weatherJsonStr);
            JSONArray weatherArray = weatherJson.getJSONArray(LIST);

            String[] jsonResultArray = new String[daysNum-1];

            for(int i = 0; i < weatherArray.length(); i++) {
                String day;
                String weatherDescription;

                JSONObject weatherDay = weatherArray.getJSONObject(i); //get the day for which the forecast is

                //Convert day from long to a human-readable format
                day = convertDate(weatherDay.getLong(DATETIME));

                //Get the weather description from the weather object inside the JSON
                JSONObject weatherObject = weatherDay.getJSONArray(WEATHER).getJSONObject(0);
                weatherDescription = weatherObject.getString(WEATHER_DESCRIPTION);

                JSONObject temperatureObject = weatherDay.getJSONObject(TEMPERATURE);
                double low = temperatureObject.getDouble(MIN);
                double high = temperatureObject.getDouble(MAX);

                //Take current day detailed data
                if (i == 0) {
                    todayDate = day;
                    todayTemperatureHigh = formatTemperatureToday(getActivity(), high);
                    todayTemperatureLow = formatTemperatureToday(getActivity(), low);
                    todayWeatherDescription = weatherDescription.substring(0,1).toUpperCase()
                            + weatherDescription.substring(1); //Capitalize first letter of the description
                    todayHumidity = "Humidity: " + weatherDay.getString(HUMIDITY) + "%";
                    todayPressure = "Pressure: " + weatherDay.getString(PRESSURE) + "hPa";
                    todayClouds = "Clouds: " + weatherDay.getString(CLOUDS) + "%";
                    todayWind = formatWind(getActivity(), weatherDay.getDouble(WIND_SPEED), weatherDay.getDouble(WIND_DIRECTION));
                    todayWeatherCode = weatherObject.getString(WEATHER_CODE);
                }
                else {
                    //the result array contains the strings for the forecast listView
                    //It is -1 day, as the first day's details are displayed above the listView
                    jsonResultArray[i-1] = day + " - " + weatherDescription + " - " + formatTemperature(getActivity(), low, high);
                }
            }

            for (String str : jsonResultArray)
                Log.v(log_title, "List entry" + str);

            return jsonResultArray;
        }

        @Override
        protected String[] doInBackground(String... params) {

            int daysNum = 8; //Re-declare for easier alteration, as the variable is used
            //within this function multiple times

            //Outside try method, so that they can be closed later
            HttpURLConnection urlConnection = null;
            BufferedReader bufferReader = null;

            //Will contain the raw JSON response as a string.
            String forecastJsonStr = null;

            if (params.length == 0)
                return null;

            try {
                //Build API query URL
                final String api_url = "http://api.openweathermap.org/data/2.5/forecast/daily?";
                final String query_param = "q";
                final String units_type_param = "units";
                final String days_num_param = "cnt";
                final String data_type_param = "mode";

                Uri buildUri = Uri.parse(api_url).buildUpon()
                        .appendQueryParameter(query_param, params[0])
                        .appendQueryParameter(units_type_param, "metric")
                        .appendQueryParameter(days_num_param, Integer.toString(daysNum))
                        .appendQueryParameter(data_type_param, "json").build();

                URL url = new URL(buildUri.toString());

                Log.v(log_title, "URI" + buildUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder stringBuilder = new StringBuilder();
                if (inputStream == null) {
                    return null;
                }
                bufferReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferReader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary and it won't affect parsing
                    // But it does make debugging easier when the completed stringBuilder
                    // is logged for debugging.
                    stringBuilder.append(line).append("\n");
                }

                if (stringBuilder.length() == 0) {
                    return null;
                }
                forecastJsonStr = stringBuilder.toString();
                Log.v(log_title, "JSON data" + forecastJsonStr);

            } catch (IOException e) {
                Log.e(log_title, "Error ", e);
                return null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (bufferReader != null) {
                    try {
                        bufferReader.close();
                    } catch (final IOException e) {
                        Log.e(log_title, "Error in closing buffer", e);
                    }
                }
            }

            try {
                return extractJsonData(forecastJsonStr, daysNum);
            }
            catch (JSONException e) {
                Log.e(log_title, e.getMessage(), e);
                e.printStackTrace();
            }
            //return null if the data parsing didn't work
            return null;
        }

        @Override //Pass the API data to the adapter
        protected void onPostExecute(String[] jsonResultArr) {
            TextView dateView = (TextView) getActivity().findViewById(R.id.weather_forecast_date);
            dateView.setText(todayDate); //Set today's date

            TextView humidityView = (TextView) getActivity().findViewById(R.id.weather_forecast_humidity);
            humidityView.setText(todayHumidity);

            TextView windView = (TextView) getActivity().findViewById(R.id.weather_forecast_wind);
            windView.setText(todayWind);

            TextView weatherDescriptionView = (TextView) getActivity().findViewById(R.id.weather_forecast_state);
            weatherDescriptionView.setText(todayWeatherDescription);

            TextView temperatureHighView = (TextView) getActivity().findViewById(R.id.weather_forecast_temp_max);
            temperatureHighView.setText(todayTemperatureHigh);

            TextView temperatureLowView = (TextView) getActivity().findViewById(R.id.weather_forecast_temp_min);
            temperatureLowView.setText(todayTemperatureLow);

            TextView pressureView = (TextView) getActivity().findViewById(R.id.weather_forecast_pressure);
            pressureView.setText(todayPressure);

            TextView cloudsView = (TextView) getActivity().findViewById(R.id.weather_forecast_clouds);
            cloudsView.setText(todayClouds);

            ImageView weatherImage = (ImageView) getActivity().findViewById(R.id.weather_forecast_image);
            weatherImage.setImageResource(getWeatherIcon(todayWeatherCode));

            if (jsonResultArr != null) {
                forecastAdapter.clear();
                forecastAdapter.addAll(jsonResultArr);
            }
        }
    }
}